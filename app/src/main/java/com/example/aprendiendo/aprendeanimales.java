package com.example.aprendiendo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class aprendeanimales extends AppCompatActivity implements View.OnClickListener{

    private android.widget.TextView txtNameAnimal;
    private android.widget.TextView txtOption1Animal1;
    private android.support.v7.widget.CardView Option1Animal1;
    private android.widget.TextView txtOption1Animal2;
    private android.support.v7.widget.CardView Option1Animal2;
    private android.widget.TextView txtOption1Animal3;
    private android.support.v7.widget.CardView Option1Animal3;
    private android.widget.TextView txtOption1Animal4;
    private android.support.v7.widget.CardView Option1Animal4;
    private android.widget.Button btncalificar;

    private String op1, op2, op3, op4;
    private int op_select = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aprendeanimales);
        this.btncalificar = (Button) findViewById(R.id.btncalificar);
        this.Option1Animal4 = (CardView) findViewById(R.id.Option1Animal4);
        this.txtOption1Animal4 = (TextView) findViewById(R.id.txtOption1Animal4);
        this.Option1Animal3 = (CardView) findViewById(R.id.Option1Animal3);
        this.txtOption1Animal3 = (TextView) findViewById(R.id.txtOption1Animal3);
        this.Option1Animal2 = (CardView) findViewById(R.id.Option1Animal2);
        this.txtOption1Animal2 = (TextView) findViewById(R.id.txtOption1Animal2);
        this.Option1Animal1 = (CardView) findViewById(R.id.Option1Animal1);
        this.txtOption1Animal1 = (TextView) findViewById(R.id.txtOption1Animal1);
        this.txtNameAnimal = (TextView) findViewById(R.id.txtNameAnimal);

        Option1Animal1.setOnClickListener(this);
        Option1Animal2.setOnClickListener(this);
        Option1Animal3.setOnClickListener(this);
        Option1Animal4.setOnClickListener(this);

        btncalificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calificar(op_select);
            }
        });

    }

    private void calificar(int op_selec) {

        switch (op_selec){
            case 1:
                Option1Animal1.setCardBackgroundColor(0xFF43a047);
                break;
            case 2:
                Option1Animal2.setCardBackgroundColor(0xFFd50000);
                Option1Animal1.setCardBackgroundColor(0xFF43a047);
                break;
            case 3:
                Option1Animal3.setCardBackgroundColor(0xFFd50000);
                Option1Animal1.setCardBackgroundColor(0xFF43a047);
                break;
            case 4:
                Option1Animal4.setCardBackgroundColor(0xFFd50000);
                Option1Animal1.setCardBackgroundColor(0xFF43a047);
                break;
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.Option1Animal1 :
                op_select = 1;
                Option1Animal1.setCardBackgroundColor(0xFFffeb3b);
                Option1Animal2.setCardBackgroundColor(0xFFededed);
                Option1Animal3.setCardBackgroundColor(0xFFededed);
                Option1Animal4.setCardBackgroundColor(0xFFededed);
                break;
            case R.id.Option1Animal2 :
                op_select = 2;
                Option1Animal1.setCardBackgroundColor(0xFFededed);
                Option1Animal2.setCardBackgroundColor(0xFFffeb3b);
                Option1Animal3.setCardBackgroundColor(0xFFededed);
                Option1Animal4.setCardBackgroundColor(0xFFededed);
                break;
            case R.id.Option1Animal3 :
                op_select = 3;
                Option1Animal1.setCardBackgroundColor(0xFFededed);
                Option1Animal2.setCardBackgroundColor(0xFFededed);
                Option1Animal3.setCardBackgroundColor(0xFFffeb3b);
                Option1Animal4.setCardBackgroundColor(0xFFededed);
                break;
            case R.id.Option1Animal4 :
                op_select = 4;
                Option1Animal1.setCardBackgroundColor(0xFFededed);
                Option1Animal2.setCardBackgroundColor(0xFFededed);
                Option1Animal3.setCardBackgroundColor(0xFFededed);
                Option1Animal4.setCardBackgroundColor(0xFFffeb3b);
                break;
            default:break;
        }
    }
}
